const {readData, upperCase, lowerCase, readNewFile, deleteFiles} = require("../problem2.js")


readData().then((data) => {
    console.log("Data reading is done successfully")
    return upperCase(data)
}).then( dataUpperCase => {
    console.log(`data is written successfully to a new file : "sample1.txt"`)
    return lowerCase(dataUpperCase)
}).then(dataLowerCase => {
    console.log(`data is wriiten successfully to a new file : "sample2.txt"`)
    return readNewFile(dataLowerCase)
}).then(() => {
    console.log(`data is wriiten successfully to a new file : "sample3.txt"`)
    return deleteFiles()
}).then(message => {
    console.log(message)
}).catch(error => console.log(error))
