const {createFolder,createFiles,deleteFiles} = require("../problem1.js")

createFolder()
.then(response => {
    console.log(response)
    return createFiles(5)
}).then(()=>{
    return deleteFiles(5)
}).then((response)=> console.log(response))
.catch(error => console.log(error))