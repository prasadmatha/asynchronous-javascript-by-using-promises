/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs")    //importing "fs" module.
const filePath = "../../data/lipsum.txt"    //path of "lipsum.txt" file

//1. Reading data of lipsum.txt
function readData() {
    const readFile = new Promise((resolve, reject) => {
        fs.readFile(filePath, "utf-8", function (err, data) { // read "lipsum.txt"
            if (err) {
                reject(err)
            }
            else {
                resolve(data)
            }
        })
    })
    return readFile
}


//2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
function upperCase(data) {
    return new Promise((resolve, reject) => {
        let convertedTextUpperCase = data.toUpperCase()
        fs.writeFile("sample1.txt", convertedTextUpperCase, function (err) {
            if (err) {
                reject(err)
            }
            else {
                fs.writeFile("fileNames.txt", "sample1.txt", function (err) {
                    if (err) {
                        reject(err)
                    }
                    else {
                        resolve(convertedTextUpperCase)
                    }
                })
            }
        })
    })
}


//3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
function lowerCase(data) {
    return new Promise((resolve, reject) => {
        let convertedTextLowerCase = data.toLowerCase()
        convertedTextLowerCase = convertedTextLowerCase.split(" ")
        convertedTextLowerCase = JSON.stringify(convertedTextLowerCase)
        fs.writeFile("sample2.txt", convertedTextLowerCase, function (err) {
            if (err) {
                reject(err)
            }
            else {
                fs.appendFile("./fileNames.txt", "\nsample2.txt", function (err) {
                    if (err) {
                        reject(err)
                    }
                    else {
                        resolve(convertedTextLowerCase)
                    }
                })
            }
        })
    })
}


//4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
function readNewFile(data) {
    return new Promise((resolve, reject) => {
        fs.readFile("./sample2.txt", "utf-8", (err, data) => {
            if (err) {
                reject(err)
            }
            else {
                data = JSON.parse(data)
                data = data.sort()
                data = JSON.stringify(data)
                fs.writeFile("./sample3.txt", data, (err => {
                    if (err) {
                        reject(err)
                    }
                    else {
                        fs.appendFile("./fileNames.txt", "\nsample3.txt", (err => {
                            if (err) {
                                reject(err)
                            }
                            else {
                                resolve(data)
                            }
                        }))
                    }
                }))
            }
        })
    })
}


//5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
function deleteFiles() {
    return new Promise((resolve,reject)=>{
        fs.readFile("./fileNames.txt", "utf-8", (err, data) => {
            if (err) {
                reject(err)
            } else {
                data = data.split("\n")
                for (let i = 0; i < data.length; i++) {
                    fs.unlink(`${data[i]}`, (err => {
                        if (err) {
                            reject(err)
                        } else {
                            console.log(`${data[i]} is deleted successfully`)
                        }
                    }))
                }
            }
        })
    }) 
}




module.exports = {readData,upperCase,lowerCase, readNewFile, deleteFiles}


