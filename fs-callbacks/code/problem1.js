/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require("fs"); //importing "fs" module.

function createFolder() {
    const createFolder = new Promise((resolve, reject) => {
        fs.mkdir("./randomJSONFiles", (err => {
            if (err) {
                reject(err)
            }
            else {
                resolve(`Folder "randomJSONFiles" is created successfully`)
            }
        }))
    })
    return createFolder
}




function createFiles(n) {
    return new Promise(function(resolve,reject){
        for(let i = 1; i <= n;i++){
            fs.writeFile(`./randomJSONFiles/sample${i}.json`, "", (err => {
                if(err){
                    reject(err)
                }
                else{
                    console.log(`sample${i}.json is created successfully`)
                }
                if (i == n){
                    resolve("Files are created successfully")
                }
            }))
            
        }
    })
}

function deleteFiles(n) {
    return new Promise(function(resolve,reject){
        for(let j = 1; j <= n; j++){
            fs.unlink(`./randomJSONFiles/sample${j}.json`, (err => {
                if(err){
                    reject(err)
                }
                else{
                    console.log(`sample${j}.json is deleted successfully`)
                }
                if (j == n){
                    resolve("Done with promises successfully")
                }
            }))
            
        }
    })
}


module.exports = {createFolder,createFiles,deleteFiles}