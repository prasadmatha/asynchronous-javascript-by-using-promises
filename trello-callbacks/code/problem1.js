/* 
    Problem 1: Write a function that will return a particular board's information based on 
    the boardID that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/

function getBoardsInfo(boards,id) {
    return new Promise(function (resolve,reject){
        setTimeout(()=>{
            const result = boards.filter(each => each.id === id) // getting board info based on id
            resolve(result)
        },2000)
        
    })
    
    
}

module.exports = getBoardsInfo  //exporting getBoardsInfo