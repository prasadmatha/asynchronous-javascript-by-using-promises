/* 
    Problem 4: Write a function that will use the previously written functions 
    to get the following information. You do not need to pass control back to 
    the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

function getInfo(
  getBoardsInfo,
  getListsInfo,
  getCardsInfo,
  boards,
  lists,
  cards,
  name
) {
  //declaring getInfo to make use of functions in problem 1, 2 & 3.
  return new Promise((resolve, reject) => {
    resolve(() => {
      let boardData = boards.filter((each) => each.name === name); //getting board data based on name.

      let id = boardData[0].id; // getting board id and assigning to variable "id"

      const resultBoards = getBoardsInfo(boards, id); //getting boards info
      resultBoards
        .then((response) => {
          console.log(response);
        })
        .catch((error) => console.log(error));

      const resultLists = getListsInfo(lists, id); // getting lists info
      resultLists
        .then((response) => console.log(response))
        .catch((error) => console.log(error));

      let arrayLists = Object.entries(lists); //converting lists into arrays

      const listsInfo = arrayLists.map((each) => [...each[1]]).flat(); // getting elements in lists

      const cardId = listsInfo.reduce((acc, curr) => {
        // initializing cardId with cardId in lists
        if (curr.name === "Mind") {
          acc.push(curr.id);
        }
        return acc;
      }, []);

      const resultCards = getCardsInfo(cards, cardId); //getting cards info
      resultCards
        .then((response) => console.log(response))
        .catch((error) => console.log(error));
    });
  });
}

module.exports = getInfo; //exporting getInfo
