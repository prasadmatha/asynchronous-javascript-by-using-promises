
const getBoardsInfo = require("../problem1.js") //importing getBoardsInfo from "problem1.js"

const getListsInfo = require("../problem2.js") //importing getListsInfo from "problem2.js"

const getCardsInfo = require("../problem3.js") //importing getCardsInfo from "problem3.js"

const boards = require("../../data/boards.json") // imporing boards data

const lists = require("../../data/lists.json") // imporing lists data

const cards = require("../../data/cards.json") //importing cards data

const getInfo = require("../problem4.js") //importing getInfo from "problem4.js"

getInfo(getBoardsInfo,getListsInfo,getCardsInfo,boards,lists,cards,"Thanos")
.then(response => {
    response()
})
.catch(error => {
    console.log(error)
}) //calling "getInfo"

