const getListsInfo = require("../problem2.js") // importing getListsInfo

const lists = require("../../data/lists.json") //importing lists data

getListsInfo(lists,"mcu453ed")
.then(response => console.log(response))
.catch(error => console.log(error))
