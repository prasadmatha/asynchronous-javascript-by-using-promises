/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
function getCardsInfo(cards, id) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (!Array.isArray(id)) id = Array(id)   //checking that is given id is a string or an array and converting that to array if not an array.

            const result = id.reduce((acc, curr) => {   //getting cards info by id


                if (cards[curr]) {

                    acc.push(...cards[curr])

                }

                return acc

            }, [])

            resolve(result) //returning result
        },2000)
    })
}




module.exports = getCardsInfo   //exporting getCardsInfo